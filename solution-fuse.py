#!/usr/bin/env python
#
#    Copyright (C) 2023  Bernhard Guillon <Bernhard.Guillon@begu.org>
#
#    Based on pyvcproj example code with
#    Copyright (C) 2006  Andrew Straw  <strawman@astraw.com>
#
#    This program can be distributed under the terms of the GNU LGPL.
#    See the file COPYING.
#

import sys
import os
import stat
import errno
import fuse
from fuse import Fuse
import vcproj.filters
import vcproj.solution
from bigtree import Node, find_path, find_full_path, copy_nodes_from_tree_to_tree

fuse.fuse_python_api = (0, 2)


class SolutionStat(fuse.Stat):

    def __init__(self, file):
        file_stat = os.stat(file)
        self.st_mode = 0
        self.st_ino = 0
        self.st_dev = 0
        self.st_nlink = 0
        self.st_uid = file_stat.st_uid
        self.st_gid = file_stat.st_gid
        self.st_size = 0
        self.st_atime = file_stat.st_atime
        self.st_mtime = file_stat.st_mtime
        self.st_ctime = file_stat.st_ctime


class SolutionFS(Fuse):

    def __init__(self, *args, **kw):
        Fuse.__init__(self, *args, **kw)
        self.solution_file = ""

    def main(self, *a, **kw):
        self.rootdir = os.path.dirname(self.solution_file)
        self.solution = vcproj.solution.parse(self.solution_file)
        self.tree = Node("solution")
        self.generate_solution_tree()
        return Fuse.main(self, *a, **kw)

    def convert_seperator(self, token):
        return token.replace("\\", str(os.sep))

    def generate_solution_tree(self):
        for project_file in self.solution.project_files():
            filter_file = os.path.join(
                    os.path.dirname(self.solution_file),
                    self.convert_seperator(project_file) + ".filters"
                    )
            filters = vcproj.filters.parse(filter_file)
            tree = filters.get_tree()
            Node(tree.name, parent=self.tree)
            copy_nodes_from_tree_to_tree(
                    from_tree=tree,
                    to_tree=self.tree,
                    from_paths=[tree.name],
                    to_paths=[str(self.tree.name) + "/" + str(tree.name)],
                    merge_children=True,
                    )
            # FIXME: find out why copy_nodes_from_tree_to_tree does not copy
            #        the attribute of the root node. For now just manualy
            #        copy the type.
            n = find_full_path(
                    self.tree,
                    str(self.tree.name) + "/" + str(tree.name)
                    )
            n.Type = tree.Type

    def getattr(self, path):
        st = SolutionStat(self.solution_file)
        if path == '/':
            st.st_mode = stat.S_IFDIR | 0o755
            st.st_nlink = 2
        elif path == "/" + str(self.tree.name):
            st.st_mode = stat.S_IFDIR | 0o755
            st.st_nlink = 1
        else:
            p = find_full_path(self.tree, path)
            if p.name:
                if p.Type == "Filter" or p.Type == "Project":
                    st.st_mode = stat.S_IFDIR | 0o755
                    st.st_nlink = 1
                else:
                    st.st_mode = stat.S_IFLNK | 0o755
                    st.st_nlink = 1
                    st.st_size = len(os.path.join(self.rootdir, p.realpath))
            else:
                return -errno.ENOENT
        return st

    def readdir(self, path, offset):
        if path == "/":
            for r in '.', '..':
                yield fuse.Direntry(r)
            yield fuse.Direntry(self.tree.name)
        else:
            node = find_path(self.tree, path)
            for p in node.children:
                yield fuse.Direntry(p.name)

    def readlink(self, path):
        p = find_full_path(self.tree, path)
        if not p.Type == "Filter" or p.Type == "Project":
            return p.realpath
        return -errno.ENOENT


def main():

    usage = """
Userspace solution filesystem

""" + Fuse.fusage
    server = SolutionFS(
            version="%prog " + fuse.__version__,
            usage=usage,
            dash_s_do='setsingle'
            )
    server.parser.add_option(
            mountopt="solution_file",
            help="mount given soltion file"
            )
    server.parse(values=server, errex=1)

    try:
        if server.fuse_args.mount_expected():
            if os.path.isfile(server.solution_file):
                server.solution_file = os.path.abspath(server.solution_file)
            else:
                raise OSError("file does not exist")
    except OSError:
        if server.solution_file:
            print(
                "Can't find soltuion_file: " + server.solution_file,
                file=sys.stderr
                )
        else:
            print("No solution_file given add -o solution_file=PATH")
        sys.exit(1)

    server.main()


if __name__ == '__main__':

    main()
