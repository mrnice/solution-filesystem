# Fuse based solution filesystem

This is a Microsoft Solution filesystem in user space.

## Project goals

Current goal is to find out if a fuse based filesystem for Microsoft solution files is a good idea. I head this idea for a long time in my head and just want to experiment with it.

## Project idea

The idea is to mount a solution file and map the filter representation into the mounted directory. For that we use symlinks to the real files. On a terminal you gain the same abstraction as in the solution tree of Visual Studio. This hopefully helps to move files between filters and projects with simple file operations like cp move ln -s and so on. While the filesystem takes care of creating and removing Filter entries with guids. With this file representation it might also be easy to get the right parameters for msbuild to just compile a single file. Later I want to experiment with either xattr or json files representing the configuration, compiler, linker and so on.

As bigtree is a nice representation of such a tree and can contain attributes per node we use it as the backing data structure.

## Further ideas

Create a .filename.cpp.compile file which ssh into the windows machine to a given path runs vcall.bat and run the single compile command or project.
Or add a script which does this with the given information. Do the same for projects with different configurations. Or try to add it to xattr.

## Current status

Current status of the project is that we can mount the test.sln file. The current output of tree to this is:

```bash
tree
.
├── baz
│   └── solution
│       ├── lib1
│       │   ├── Header Files
│       │   │   ├── stdafx.h -> /home/foobar/solution-fuse/test_solution/lib1/stdafx.h
│       │   │   └── targetver.h -> /home/foobar/solution-fuse/test_solution/lib1/targetver.h
│       │   ├── ReadMe.txt -> /home/foobar/solution-fuse/test_solution/lib1/ReadMe.txt
│       │   ├── Resource Files
│       │   └── Source Files
│       │       ├── dllmain.cpp -> /home/foobar/solution-fuse/test_solution/lib1/dllmain.cpp
│       │       ├── lib1.cpp -> /home/foobar/solution-fuse/test_solution/lib1/lib1.cpp
│       │       └── stdafx.cpp -> /home/foobar/solution-fuse/test_solution/lib1/stdafx.cpp
│       ├── lib2
│       │   ├── Header Files
│       │   │   ├── stdafx.h -> /home/foobar/solution-fuse/test_solution/lib2/stdafx.h
│       │   │   └── targetver.h -> /home/foobar/solution-fuse/test_solution/lib2/targetver.h
│       │   ├── ReadMe.txt -> /home/foobar/solution-fuse/test_solution/lib2/ReadMe.txt
│       │   ├── Resource Files
│       │   └── Source Files
│       │       └── stdafx.cpp -> /home/foobar/solution-fuse/test_solution/lib2/stdafx.cpp
│       └── test
│           ├── Header Files
│           │   ├── Resource.h -> /home/foobar/solution-fuse/test_solution/test/Resource.h
│           │   ├── stdafx.h -> /home/foobar/solution-fuse/test_solution/test/stdafx.h
│           │   ├── targetver.h -> /home/foobar/solution-fuse/test_solution/test/targetver.h
│           │   └── test.h -> /home/foobar/solution-fuse/test_solution/test/test.h
│           ├── ReadMe.txt -> /home/foobar/solution-fuse/test_solution/test/ReadMe.txt
│           ├── Resource Files
│           │   ├── small.ico -> /home/foobar/solution-fuse/test_solution/test/small.ico
│           │   ├── test.ico -> /home/foobar/solution-fuse/test_solution/test/test.ico
│           │   └── test.rc -> /home/foobar/solution-fuse/test_solution/test/test.rc
│           └── Source Files
│               ├── stdafx.cpp -> /home/foobar/solution-fuse/test_solution/test/stdafx.cpp
│               └── test.cpp -> /home/foobar/solution-fuse/test_solution/test/test.cpp
```

Later we want to support to also only mount a project.

We can also already resolve the symbolic links so cat is possible

```bash
cat baz/solution/lib1/ReadMe.txt
========================================================================
    DYNAMIC LINK LIBRARY : lib1 Project Overview
========================================================================

AppWizard has created this lib1 DLL for you.

This file contains a summary of what you will find in each of the files that
make up your lib1 application.


lib1.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

lib1.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).

lib1.cpp
    This is the main DLL source file.

    When created, this DLL does not export any symbols. As a result, it
    will not produce a .lib file when it is built. If you wish this project
    to be a project dependency of some other project, you will either need to
    add code to export some symbols from the DLL so that an export library
    will be produced, or you can set the Ignore Input Library property to Yes
    on the General propert page of the Linker folder in the project's Property
    Pages dialog box.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named lib1.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
```

The filesystem is currently readonly and use the user and group information of the .sln file as owner.

## Requirements

This project requires bigtree and fuse you can install it as usual with

```bash
pip install bigtree
```

It is recommended to use any python virtual environment.

## Usage

Mounting

```bash
mkdir baz
./solution-fuse.py baz -o solution_file=test_solution/test.sln
```

Unmounting

```bash
umount baz
```

## License

### solution-fuse.py

```text
Copyright (C) 2023  Bernhard Guillon <Bernhard.Guillon@begu.org>
Based on pyvcproj example code with
Copyright (C) 2006  Andrew Straw  <strawman@astraw.com>

This program can be distributed under the terms of the GNU LGPL-2.1.
See the file LICENSE.
```

### pyvcxproj

```text
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org>

```

## Contribution

Your ideas and contributions are very welcome. Just use a PR or open up an issue.

Happy hacking :)

